//TODO: Error handling for same stone being included on both sides of the scale

#include <iostream>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include "stone.hh"

int jewelStone;
int userGuess;
int leftSideWeight;
int rightSideWeight;

std::vector<stone> mysteryStones(9);
std::vector<int> rightSideOfScale;
std::vector<int> leftSideOfScale;

void start(){
  std::cout << "\n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n";
  std::cout << "On an archeological dig near Egypt, you discover an ancient oil lamp. \n";
  std::cout << "You rub it and a genie pops out. Nine normal looking rocks and a scale appear next to the geanie.\n";
  std::cout << "\n";
  std::cout << "\"Here are 9 numbered stones. One of them contains a jewel worth more than you could imagine.\" \n";
  std::cout << "\"Eight of the stones weigh the same, but the stone with the jewel weighs slightly more.\" \n";
  std::cout << "\"You may use the scale two times. Then you must choose which stone you think has the jewel.\"";
  std::cout << "\n";
}

void leftSide(){
  leftSideWeight = 0;
  leftSideOfScale.clear();

  std::cout << "INPUT: ";

  std::string tempString;
  getline(std::cin, tempString);
  int tempInt;
  std::stringstream tempStream(tempString);

  while(tempStream >> tempInt){
      leftSideOfScale.push_back(tempInt);
  }

  if(leftSideOfScale.size() == 1) std::cout << "\n\"Alright I am putting stone ";
  else std::cout << "\n\"Alright I am putting stones ";

  for(int i = 0; i < leftSideOfScale.size(); ++i){
    std::cout << leftSideOfScale[i];
    leftSideWeight += mysteryStones.at(leftSideOfScale[i]-1).getWeight();
    if(i < leftSideOfScale.size()-1){
      std::cout << ", ";
    }
  }
  std::cout << " on the left side of the scale.\"\n";
}

void rightSide(){
  rightSideWeight = 0;
  rightSideOfScale.clear();

  std::cout << "INPUT: ";

  std::string tempString;
  getline(std::cin, tempString);
  int tempInt;
  std::stringstream tempStream(tempString);

  while(tempStream >> tempInt){
      rightSideOfScale.push_back(tempInt);
  }

  if(rightSideOfScale.size() == 1) std::cout << "\n\"Alright I am putting stone ";
  else std::cout << "\n\"Alright I am putting stones ";

  for(int i = 0; i < rightSideOfScale.size(); ++i){
    std::cout << rightSideOfScale[i];
    rightSideWeight += mysteryStones.at(rightSideOfScale[i]-1).getWeight();
    if(i < rightSideOfScale.size()-1){
      std::cout << ", ";
    }
  }
  std::cout << " on the right side of the scale.\"\n";
}

void moveScales(){
  //std::cout << "the left side weighs " << leftSideWeight << std::endl;
  //std::cout << "the right side weighs " << rightSideWeight << std::endl;

  if(leftSideWeight < rightSideWeight){
    std::cout << "\nThe scale tips to the right side.\n";
  }
  else if(leftSideWeight > rightSideWeight){
    std::cout << "\nThe scale tips to the left side.\n";
  }
  else if(leftSideWeight == rightSideWeight){
    std::cout << "\nThe scale does not tip over to either side.\n";
  }
}

void end(){
  std::cout << "\"You must now guess which numbered stone contains the jewel.\" \n";
  std::cout << "\n\"What is your answer?\" \n";
  std::cout << "INPUT: ";
  std::cin >> userGuess;

  if(userGuess == jewelStone + 1){
    std::cout << "\n\"You're right! The stone with the jewel is stone " << jewelStone + 1 << ".\" \n";

  }
  else{
    std::cout << "\n\"You're wrong! The stone with the jewel is stone " << jewelStone + 1 << ".\" \n";
  }
  std::cout << "\nGAME OVER \n";
}

int main (){
  srand((unsigned) time(0));
  for(int i = 0; i < 9; ++i){
    mysteryStones[i].setValues(i+1, 1);
  }
  jewelStone = rand() % 9;
  //std::cout << "The stone with the jewel is " << jewelStone + 1 << std::endl;
  mysteryStones[jewelStone].setValues(jewelStone+1, 2);

  start();

  for(int turn = 0; turn < 2; ++turn){
    std::cout << "\n\"Which stones you would like to put on the left side of the scale?\"\n";
    if(turn == 0) std::cout << "\nExample: if you wanted stones 4, 5, 6, and 7 you would enter \"4 5 6 7\"\n";
    leftSide();
    std::cout << "\n\"Which stones you would like to put on the right side of the scale?\"\n";
    rightSide();
    moveScales();// looks at weight of both sides of scale and tells user what happens
    std::cout << "\n\"Your number of turns left is " << 1 - turn << ".\"\n";
  }

  std::cout << "\n";
  end();
  return 0;
}
