This is an interactive terminal application inspired by the first riddle in the first chapter
of the 3rd edition of The Heart of Mathematics by Burger and Starbird.

To compile type this into your terminal:
g++ -o MeanieGenie MeanieGenie.cc stone.cc 
