#include "stone.hh"

void stone::setValues(int number, int weight){
  this->number = number;
  this->weight = weight;
  //std::cout << "Stone " << number << " weighs " << weight << "g.\n";
}

int stone::getWeight(){
    return this->weight;
}
