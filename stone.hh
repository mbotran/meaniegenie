#define _stone_

class stone{
  private:
    int number;
    int weight;

  public:
    void setValues(int, int);
    int getWeight();
};
